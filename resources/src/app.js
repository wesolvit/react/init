import React from 'react';

const App = () => (
  <section id="application-container">
    <header>
      <h1>
        My application named&nbsp;
        <i>%name</i>
      </h1>
    </header>
    <div id="application-content">
      <pre>
        It&apos;s your turn to build something amazing.
      </pre>
    </div>
  </section>
);

App.displayName = 'App: %name';

export default App;
