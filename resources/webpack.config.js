'use strict';

const Dotenv = require('dotenv-webpack');
const ESLintPlugin = require('eslint-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssPlugin = require('mini-css-extract-plugin');
const postcssPresetEnv = require('postcss-preset-env');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const fs = require('fs');
const path = require('path');

const appConfig = JSON.parse(fs.readFileSync('./.app.config.json').toString());
const plugins = [];

if (fs.existsSync(path.resolve('./.env'))) {
  plugins.push(new Dotenv({
    systemvars: true,  // load all the predefined 'process.env' variables
    // which will trump anything local per dotenv specs.
  }));
}

module.exports = {
  entry: ['./src/index.js', './src/styles/style.sass'],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js',
    publicPath: 'publicPathPlaceholder',
  },
  resolve: {
    'extensions': ['.js', '.jsx', '.ts', '.tsx'],
  },
  optimization: {
    // We no not want to minimize our code.
    minimize: false,
  },
  module: {
    rules: [
      {
        test: /\.[tj]sx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.svg$/,
        use: {
          loader: 'raw-loader',
        },
      },
      {
        test: /\.(jpg|png|woff|woff2|eot|ttf|jpeg)$/,
        use: {
          loader: 'file-loader?limit=100000&name=images/[name].[ext]',
        },
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
          },
        ],
      },
      {
        test: /\.css?$/,
        use: [
          {loader: 'style-loader'},
          {
            loader: 'resolve-url-loader',
          },
          {
            loader: 'css-loader',
          },
          {
            loader: 'postcss-loader', options: {
              ident: 'postcss',
              plugins: () => [postcssPresetEnv(/* pluginOptions */)],
            },
          },
        ],
      },
      {
        test: /\.s[ac]ss$/,
        use: [
          {loader: MiniCssPlugin.loader},
          {loader: 'css-loader'},
          {loader: 'resolve-url-loader'},
          {loader: 'sass-loader'},
        ],
      },
    ],
  },
  // mode: process.env.ENV_MODE,
  devtool: process.env.ENV_MODE === 'production' ? false : 'source-map',
  devServer: {
    historyApiFallback: true,
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin(),
    new BrowserSyncPlugin({
      host: '0.0.0.0',
      port: 3000,
      server: {baseDir: [path.resolve(__dirname, 'build')]},
    }),
    new ESLintPlugin({
      eslintPath: require.resolve('eslint'),
      emitWarning: true,
      failOnError: true,
      useEslintrc: true,
      files: './src',
      extensions: ["js", "jsx", "ts", "tsx"],
    }),
    ...plugins,
    new MiniCssPlugin(),

    new HtmlWebPackPlugin({
      template: 'public/index.ejs',
      title: appConfig.react.app.title,
      publicPath: '/',
      manifest: 'public/manifest.json',
      templateParameters: (compilation, assets, assetTags, options) => ({
        ...options,
        ...appConfig,
      }),
    }),

    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, 'public', 'manifest.json'),
          to: path.resolve(__dirname, process.env.ENV_MODE === 'production' ? 'dist' : 'build', 'manifest.json'),
        },
        {
          from: path.resolve(__dirname, 'public', 'assets'),
          to: path.resolve(__dirname, process.env.ENV_MODE === 'production' ? 'dist' : 'build', 'assets'),
          noErrorOnMissing: true,
        },
      ],
    }),
  ],
};
