const typescriptEslintAll = require('@typescript-eslint/eslint-plugin').configs.all;

module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
  ],
  parser: '@babel/eslint-parser',
  settings: {
    react: {
      pragma: 'React',
      version: 'detect',
    },
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
      classes: true,
      modules: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
    'import',
    'jsx-a11y',
  ],
  rules: {
    indent: [
      'error',
      2,
      {
        SwitchCase: 1,
      },
    ],
    'linebreak-style': [
      'error',
      'unix',
    ],
    quotes: [
      'error',
      'single',
    ],
    semi: [
      'error',
      'always',
    ],
    'prefer-destructuring': [
      'error',
      {
        object: true,
        array: false,
      },
    ],
    'no-console': [
      process.env.ENV_MODE === 'production' ? 'error' : 'off',
      {
        allow: ['error'],
      },
    ],
    'no-debugger': process.env.ENV_MODE === 'production' ? 'error' : 'off',
    'no-alert': process.env.ENV_MODE === 'production' ? 'error' : 'off',
    'prefer-template': 'error',
    'no-useless-escape': 'error',
    'key-spacing': [
      'error',
      {
        beforeColon: false,
        afterColon: true,
        mode: 'strict',
      },
    ],
    'no-useless-concat': 'error',
    'no-template-curly-in-string': 'error',
    'no-multiple-empty-lines': [
      'error',
      {
        max: 1,
        maxBOF: 0,
        maxEOF: 1,
      },
    ],
    'eol-last': ['error', 'always'],
    'comma-dangle': [
      'error',
      {
        arrays: 'always-multiline',
        objects: 'always-multiline',
        imports: 'always-multiline',
        exports: 'always-multiline',
        functions: 'never',
      },
    ],
    'import/order': [
      'error',
      {
        alphabetize: {
          order: 'asc',
          caseInsensitive: false,
        },
        groups: ['external', 'internal', 'parent', 'sibling'],
        'newlines-between': 'always',
      },
    ],
    'comma-spacing': [
      'error',
      {
        before: false,
        after: true,
      },
    ],
    'array-bracket-spacing': [
      'error',
      'never',
    ],
    'object-curly-spacing': ['error', 'never'],
    'object-shorthand': [
      'error',
      'always',
      {
        avoidQuotes: true,
        avoidExplicitReturnArrows: false,
      },
    ],
    'arrow-body-style': ['error', 'as-needed'],
    'computed-property-spacing': [
      'error',
      'never',
      {
        enforceForClassMembers: true,
      },
    ],
    'array-bracket-newline': [
      'error',
      {
        multiline: true,
        minItems: 5,
      },
    ],
    'no-extra-parens': [
      'error',
      'all',
      {
        returnAssign: false,
        enforceForNewInMemberExpressions: false,
        nestedBinaryExpressions: false,
        ignoreJSX: 'multi-line',
      },
    ],
    'arrow-parens': ['error', 'as-needed'],
    'space-in-parens': ['error', 'never'],
    'operator-linebreak': ['error', 'after'],
    'no-duplicate-case': 'error',
    curly: 'error',
    'brace-style': ['error', 'stroustrup'],
    'keyword-spacing': [
      'error',
      {
        before: true,
        after: true,
      },
    ],
    'jsx-quotes': ['error', 'prefer-double'],
    'react/prop-types': 'error',
    'react/boolean-prop-naming': ['error', {validateNested: true}],
    'react/destructuring-assignment': ['error', 'always'],
    'react/display-name': ['error', {ignoreTranspilerName: true}],
    'react/function-component-definition': [
      'error',
      {
        namedComponents: 'arrow-function',
        unnamedComponents: 'arrow-function',
      },
    ],
    'react/no-access-state-in-setstate': 'error',
    'react/no-array-index-key': 'error',
    'react/no-children-prop': 'error',
    'react/no-deprecated': 'error',
    'react/no-direct-mutation-state': 'error',
    'react/no-string-refs': 'error',
    'react/no-this-in-sfc': 'error',
    'react/no-typos': 'error',
    'react/no-unescaped-entities': 'error',
    'react/no-unsafe': 'error',
    'react/no-unused-prop-types': 'error',
    'react/no-unused-state': 'error',
    'react/prefer-es6-class': 'error',
    'react/prefer-stateless-function': ['error', {ignorePureComponents: true}],
    'react/require-render-return': 'error',
    'react/self-closing-comp': ['error', {component: true, html: true}],
    'react/sort-comp': [
      'error',
      {
        order: [
          'static-methods',
          'lifecycle',
          'everything-else',
          'render',
        ],
        groups: {
          lifecycle: [
            'displayName',
            'propTypes',
            'contextTypes',
            'childContextTypes',
            'mixins',
            'statics',
            'defaultProps',
            'constructor',
            'getDefaultProps',
            'state',
            'getInitialState',
            'getChildContext',
            'getDerivedStateFromProps',
            'componentWillMount',
            'componentDidMount',
            'componentWillReceiveProps',
            'shouldComponentUpdate',
            'componentWillUpdate',
            'getSnapshotBeforeUpdate',
            'componentDidUpdate',
            'componentDidCatch',
            'componentWillUnmount',
          ],
        },
      },
    ],
    'react/sort-prop-types': [
      'error', {
        callbacksLast: true,
        ignoreCase: false,
        requiredFirst: true,
        sortShapeProp: true,
        noSortAlphabetically: false,
      },
    ],
    'react/static-property-placement': ['error', 'static public field'],
    'react/style-prop-object': 'error',
    'react/jsx-boolean-value': ['error', 'never'],
    'react/jsx-closing-bracket-location': ['error', 'line-aligned'],
    'react/jsx-closing-tag-location': 'error',
    'react/void-dom-elements-no-children': 'error',
    'react/jsx-curly-brace-presence': ['error', {props: 'never', children: 'never'}],
    'react/jsx-curly-newline': [
      'error',
      {
        multiline: 'require',
        singleline: 'forbid',
      },
    ],
    'react/jsx-curly-spacing': [
      'error',
      {
        when: 'never',
        children: true,
      },
    ],
    'react/jsx-equals-spacing': 'error',
    'react/jsx-filename-extension': ['error', {extensions: ['.js', '.ts', 'tsx']}],
    'react/jsx-first-prop-new-line': ['error', 'multiline'],
    'react/jsx-fragments': 'error',
    'react/jsx-handler-names': ['error', {checkLocalVariables: true}],
    'react/jsx-indent': ['error', 2, {checkAttributes: true}],
    'react/jsx-indent-props': ['error', 2],
    'react/jsx-key': ['error', {checkFragmentShorthand: true}],
    'react/jsx-max-depth': ['error', {max: 7}],
    'react/jsx-max-props-per-line': ['error', {maximum: 1, when: 'multiline'}],
    'react/jsx-no-bind': ['error', {allowArrowFunctions: true}],
    'react/jsx-no-comment-textnodes': 'error',
    'react/jsx-no-duplicate-props': ['error', {ignoreCase: true}],
    'react/jsx-no-script-url': 'error',
    'react/jsx-no-target-blank': 'error',
    'react/jsx-no-useless-fragment': 'error',
    'react/jsx-one-expression-per-line': ['error', {allow: 'single-child'}],
    'react/jsx-props-no-multi-spaces': 'error',
    'react/jsx-sort-default-props': 'error',
    'react/jsx-sort-props': [
      'error', {
        shorthandLast: true,
        reservedFirst: true,
      },
    ],
    'react/jsx-tag-spacing': [
      'error',
      {
        beforeClosing: 'never',
      },
    ],
  },
  overrides: [
    {
      files: [
        '*.tsx',
        '*.ts',
      ],
      parser: '@typescript-eslint/parser',
      parserOptions: {
        project: './tsconfig.json',
        jsx: true,
        useJSXTextNode: true,
      },
      plugins: ['@typescript-eslint'],
      extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/all',
      ],
      rules: Object.assign(typescriptEslintAll.rules, {
        '@typescript-eslint/quotes': ['error', 'single'],
        '@typescript-eslint/no-type-alias': 'off',
        '@typescript-eslint/indent': [
          'error',
          2,
          {
            SwitchCase: 1,
          },
        ],
        '@typescript-eslint/no-inferrable-types': 'off',
        '@typescript-eslint/prefer-readonly-parameter-types': 'off',
        '@typescript-eslint/naming-convention': [
          'error',
          {
            selector: 'default',
            format: ['camelCase', 'snake_case', 'PascalCase', 'UPPER_CASE'],
            leadingUnderscore: 'allow',
          },
          {
            selector: 'property',
            filter: '^__html$',
            format: null,
          },
        ],
        '@typescript-eslint/no-unnecessary-condition': 'off',
        '@typescript-eslint/no-invalid-this': 'off',
        '@typescript-eslint/strict-boolean-expressions': 'off',
        '@typescript-eslint/no-empty-interface': 'off',
        '@typescript-eslint/no-implicit-any-catch': 'off',
      }),
    },
  ],
};
