'use strict';

const licenses = require('spdx-license-list/simple');

/**
 * Program info.
 *
 * @type {{
 *   options: [
 *     {
 *       isFlag: boolean,
 *       name: string,
 *       short: string,
 *       description: string,
 *       alias?: string,
 *       type: string,
 *       message: string,
 *       long: string,
 *       default?: *,
 *       when?: function(answers: {docker?: boolean, dockerType?: string}): boolean,
 *       validate?: function(answer: string): string|boolean,
 *       transformer?: function(answer: string, answers: {}, flags: {isFinal: boolean}): *,
 *       choices?: string[],
 *     }
 *   ]
 *  }}
 */
module.exports = {
  options: [
    {
      name: 'name',
      isFlag: false,
      short: '-n',
      long: '--project-name',
      description: 'The project name',
      type: 'input',
      message: 'Project name',
      alias: 'projectName',
      validate: annwser => {
        return !annwser.match(/^[a-z0-9-]+$/) ?
          'The project name can only have lower case letters, digits and hyphens.' :
          true;
      },
    },
    {
      name: 'version',
      isFlag: false,
      short: '-v',
      long: '--project-version',
      description: 'The project version',
      default: '0.0.1',
      type: 'input',
      message: 'Project version',
      alias: 'projectVersion',
    },
    {
      name: 'description',
      isFlag: false,
      short: '-d',
      long: '--desc',
      description: 'The project description',
      type: 'input',
      message: 'Project description',
      alias: 'desc',
    },
    {
      name: 'author',
      isFlag: false,
      short: '-a',
      long: '--author',
      description: 'The project author',
      type: 'input',
      message: 'Project author',
    },
    {
      name: 'license',
      isFlag: false,
      short: '-l',
      long: '--license',
      description: 'The project license',
      default: 'GPL-3.0-or-later',
      type: 'list',
      message: 'Project license',
      choices: [...licenses],
    },
    {
      name: 'docker',
      isFlag: true,
      short: '-c',
      long: '--docker',
      description: 'Use docker',
      default: false,
      type: 'confirm',
      message: 'Docker',
    },
    {
      name: 'dockerType',
      isFlag: false,
      long: '--docker-type',
      description: 'Docker type',
      default: 'docker-compose',
      type: 'list',
      message: 'Docker type',
      choices: ['docker-compose image', 'docker-compose context', 'docker run image', 'docker exec container'],
      /**
       * When method.
       *
       * @param {{docker: boolean}} answers
       *   Answers.
       *
       * @return {boolean}
       *   When to show this question.
       */
      when: answers => answers.docker,
    },
    {
      name: 'dockerComposeImage',
      isFlag: false,
      long: '--docker-compose-image',
      description: 'Image for docker-compose',
      default: 'node:alpine',
      type: 'input',
      message: 'Docker compose image',
      /**
       * When method.
       *
       * @param {{dockerType: string}} answers
       *   Answers.
       *
       * @return {boolean}
       *   When to show this question.
       */
      when: answers => answers.dockerType === 'docker-compose image',
    },
    {
      name: 'dockerComposeContext',
      isFlag: false,
      long: '--docker-compose-context',
      description: 'Build context for docker-compose',
      default: undefined,
      type: 'input',
      message: 'Path for the docker-compose build context',
      /**
       * When method.
       *
       * @param {{dockerType: string}} answers
       *   Answers.
       *
       * @return {boolean}
       *   When to show this question.
       */
      when: answers => answers.dockerType === 'docker-compose context',
      validate: answer => {
        const location = answer.trim();
        if (!location.length) {
          return 'Please provide a path with a docker-compose context ';
        }
        const fs = require('fs');
        const path = require('path');
        if (!fs.existsSync(location)) {
          return `The path: '${path.resolve(location)}'.`;
        }
        return true;
      },
      transformer: (answer, answers, flags) => {
        if (flags.isFinal) {
          const path = require('path');
          const location = answer.trim();
          return path.resolve(location);
        }
        return answer;
      },
    },
    {
      name: 'dockerRunImage',
      isFlag: false,
      long: '--docker-run-image',
      description: 'Docker image',
      default: 'node:alpine',
      type: 'input',
      message: 'Docker run image',
      /**
       * When method.
       *
       * @param {{dockerType: string}} answers
       *   Answers.
       *
       * @return {boolean}
       *   When to show this question.
       */
      when: answers => answers.dockerType === 'docker run image',
    },
    {
      name: 'dockerExecContainer',
      isFlag: false,
      long: '--docker-exec-container',
      description: 'Docker container',
      default: 'node',
      type: 'input',
      message: 'Docker exec container',
      /**
       * When method.
       *
       * @param {{dockerType: string}} answers
       *   Answers.
       *
       * @return {boolean}
       *   When to show this question.
       */
      when: answers => answers.dockerType === 'docker exec container',
    },
  ],
};
