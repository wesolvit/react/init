'use strict';

/**
 * JSON output config.
 *
 * @type {{spaces: number}}
 */
module.exports = {
  spaces: 2,
};
