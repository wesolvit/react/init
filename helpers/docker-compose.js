'use strict';

const dockerCompose = {
  version: '3.8',
  services: {
    node: {
      working_dir: '/app/build',
      volumes: [
        './:/app/build',
      ],
      ports: [
        '3000:3000',
        '5000:5000',
      ],
    },
  },
};

const aliases = `PROJECT_DIR=$(realpath $(dirname \${BASH_SOURCE}))

alias dc='docker-compose -f \${PROJECT_DIR}/docker-compose.yml'
alias dc-exec='dc exec node'
alias node="dc-exec node"
alias yarn="dc-exec yarn"
alias npm="dc-exec npm"
alias npx="dc-exec npx"
alias webpack="dc-exec webpack"
alias serve="dc-exec yarn watch"

function up () {
  current="$(pwd)"
  cd $(realpath "\${PROJECT_DIR}")
  docker-compose up -d
  cd "\${current}"
}
`;

/**
 * Write docker-compose.yml and aliases.
 *
 * @param {string} directory
 *   Target directoruy.
 */
const writeDockerComposeEnvironment = directory => {
  const yaml = require('js-yaml');
  const fs = require('fs');
  const path = require('path');

  fs.writeFileSync(path.resolve(directory, 'docker-compose.yml'), yaml.safeDump(dockerCompose));
  fs.writeFileSync(path.resolve(directory, '.aliases'), aliases);
}

module.exports = {
  /**
   * Docker compose image.
   *
   * @param {string} directory
   *   Target directory.
   * @param {string} image
   *   Image name.
   */
  dockerComposeImage: (directory, image) => {
    dockerCompose.services.node.image = image;
    writeDockerComposeEnvironment(directory);
  },
  /**
   * Docker compose context.
   *
   * @param {string} directory
   *   Target directory.
   * @param {string} context
   *   Docker compose build context.
   */
  dockerComposeContext: (directory, context) => {
    dockerCompose.services.node.build = {context: './docker/node'};
    writeDockerComposeEnvironment(directory);
    const fse = require('fs-extra');
    const fs = require('fs');
    if (!fs.existsSync(context)) {
      console.error('error context', context);
    }
    const path = require('path');
    const destination = path.resolve(directory, 'docker/node');
    if (!fs.existsSync(destination)) {
      fs.mkdirSync(destination, {recursive: true, mode: 0o755});
    }
    fse.copy(context, destination, err => {
      if (err) {
        console.error('error dcc', err);
        throw err;
      }
      console.log(`The docker-compose build context '${context}' has been copied to '${destination}'.`);
    })
  },
};
