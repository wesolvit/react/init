'use strict';

/**
 * Check if a directory is empty.
 *
 * @param {NodeJS.ErrnoException} err
 *   Error.
 * @param {[]} files
 *   List of files.
 * @param {string} directory
 *   Directory.
 *
 * @throws Error.
 */
module.exports = (err, files, directory) => {
  if (err) {
    throw err;
  }
  if (files && files.length > 0) {
    throw new Error(`The directory ${directory} is not empty`);
  }
};
