'use strict';

/**
 * Handle docker aliases.
 *
 * @param {string} directory
 *   The target directory.
 * @param {string} type
 *   Type run or exec.
 * @param {string} imageOrContainer
 *   Image name or container name.
 */
module.exports = (directory, type, imageOrContainer) => {

  const name = type === 'run' ? 'RUN' : 'EXEC';
  const args = type === 'run' ? `--rm -u '$(id -u)' -v '\${PROJECT_DIR}':'/app/build' ` : '';
  const handler = type === 'run' ? 'IMAGE' : 'CONTAINER';
  const port = type === 'run' ? ' -p 3000:3000' : '';
  const aliases = `PROJECT_DIR=$(realpath $(dirname \${BASH_SOURCE}))

${handler}=${imageOrContainer}
${name}="docker ${type} -it ${args}-w /app/build"
${name}_NODE="\${${name}} \${${handler}}"

alias node="\${${name}_NODE} node"
alias bash-node="\${${name}_NODE} bash"
alias yarn="\${${name}_NODE} yarn"
alias npm="\${${name}_NODE} npm"
alias npx="\${${name}_NODE} npx"
alias webpack="\${${name}_NODE} webpack"
alias serve="\${${name}}${port} ${type === 'run' ? `\${${handler}}` : ''} yarn watch"

`;
  const fs = require('fs');
  const path = require('path');

  fs.writeFileSync(path.resolve(directory, '.aliases'), aliases);
}

