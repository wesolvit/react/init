'use strict';

/**
 *
 * Get value name.
 *
 * @param {boolean} isFlag
 *   Is flag.
 * @param {string} name
 *   Option name.
 *
 * @return {string}
 *   Value name.
 */
const getFormattedValueName = (isFlag, name) => isFlag ? `` : ` <${name}>`;

/**
 * Get default value.
 *
 * @param {*} defaultValue
 *   Default value.
 * @return {string}
 *   Default value.
 */
const getFormattedDefaultValue = defaultValue => (typeof defaultValue === 'string' && defaultValue.length > 0) ||
(typeof defaultValue !== 'string' && defaultValue !== undefined) ?
  ` (default: ${String(defaultValue)})` : '';

/**
 * Get Formatted flags,
 * @param {string} short
 *   Short flag.
 * @param {string} long
 *   Long flag.
 *
 * @return {string}
 *   Formatted flag.
 */
const getFormattedFlags = (short, long) => [short, long].filter(item => !!item).join(', ');

module.exports = {
  getFormattedFlags,
  getFormattedDefaultValue,
  getFormattedValueName,
};
