'use strict';

const path = require('path');
const fs = require('fs');
const fse = require('fs-extra');

/**
 * Write manifest file.
 *
 * @param {string} directory
 *   Targeted directory.
 * @param {{name: string}} config
 *   Application configuration.
 */
const writeManifest = (directory, config) => {
  const manifestFile = path.resolve(directory, 'public/manifest.json');
  /** @var {{name: string, short_name: string}} manifest **/
  const manifest = require(manifestFile);
  manifest.name = config.name;
  manifest.short_name = config.name;
  fse.writeJsonSync(manifestFile, manifest, {spaces: 2});
}

/**
 * Write manifest file.
 *
 * @param {string} directory
 *   Targeted directory.
 * @param {{name: string}} config
 *   Application configuration.
 */
const writeAppFiles = (directory, config) => {
  ['index.js', 'app.js', 'styles/style.sass'].forEach(file => {
    const filePath = path.resolve(directory, 'src', file);
    fs.writeFileSync(filePath, fs
      .readFileSync(filePath)
      .toString()
      .replaceAll(/(rootId)|(%name)/g, config.name)
    );
  });
}

/**
 * File handler.
 *
 * @param {string} directory
 *   Directory.
 * @param {string} directoryFullPath
 *   Directory full path.
 * @param {*} config
 *   Configuration.
 * @return {function(NodeJS.ErrnoException=, string[]): void}
 *   Function to handle files.
 */
module.exports = (directory, directoryFullPath, config) => (err, files) => {
  if (err) {
    throw err;
  }
  const {
    docker,
    dockerType,
    dockerComposeImage,
    dockerComposeContext,
    dockerRunImage,
    dockerExecContainer,
    ...appPackageConfig
  } = config;


  if (docker) {
    const dockerAliases = require('./docker');
    switch (dockerType) {
      case 'docker-compose image':
        const {dockerComposeImage: dci} = require('./docker-compose');
        dci(directoryFullPath, dockerComposeImage);
        break;

      case 'docker-compose context':
        const {dockerComposeContext: dcc} = require('./docker-compose');
        dcc(directoryFullPath, dockerComposeContext);
        break;

      case 'docker run image':
        dockerAliases(directoryFullPath, 'run', dockerRunImage);
        break;

      case 'docker exec container':
        dockerAliases(directoryFullPath, 'exec', dockerExecContainer);
        break;
    }
  }

  files.forEach(file => {
    const fileFullPath = path.resolve(__dirname, '../resources', file);
    const destinationFullPath = path.resolve(directory, file);
    const jsonOutputConfig = require('./json-output-config');
    switch (file) {
      case 'package.json':
        const appPackage = {...require(fileFullPath), ...appPackageConfig};
        fse.outputJsonSync(destinationFullPath, appPackage, jsonOutputConfig);
        console.log(`Generate ${file} to ${directoryFullPath}`);
        break;
      case '.app.config.json':
        const title = config.description;
        const rootId = config.name;
        const appConfig = {
          ...require(fileFullPath),
          ...{
            react: {
              app: {
                title,
                rootId,
              },
            },
          },
        };
        fse.outputJsonSync(destinationFullPath, appConfig, jsonOutputConfig);
        console.log(`Generate ${file} to ${directoryFullPath}`);
        break;
      default:
        fse.copySync(fileFullPath, destinationFullPath);
        console.log(`Copy ${file} to ${directoryFullPath}`);
    }
  });

  writeManifest(directory, config);
  writeAppFiles(directory, config);

  console.log(`Your React application is ready in ${directoryFullPath}`);
};
