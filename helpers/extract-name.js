'use strict';

const path = require('path');

/**
 * Extract name from directory.
 *
 * @param {string} directory
 *   Directory path to extract the name from.
 *
 * @returns {string}
 *   The name extracted from the name.
 */
module.exports = (directory) => {
  const fullPath = path.resolve(directory);
  return path.basename(fullPath);
};
