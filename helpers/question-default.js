'use strict';

const extractNameFromDirectory = require('./extract-name');

/**
 * Provide inquiries questions.
 *
 * @param {CommanderStatic} program
 *  Program object.
 * @param {string} directory
 *   Directory
 * @param {{options: [{name: string, alias: string, default: *}]}} info
 *   Info.
 *
 * @return {[]}
 *   List of questions.
 */
module.exports = (program, directory, info) => {
  program.projectName = (program.projectName && program.projectName.length > 0) ?
    program.projectName : extractNameFromDirectory(directory);

  const {options} = info;

  const questions = [];
  options.map(question => {
    const {alias, name} = question;
    const programKey = alias ?? name;
    if (program[programKey] !== undefined && typeof program[programKey] !== 'function') {
      question.default = program[programKey];
    }
    questions.push(question);
  });

  return questions;
};
