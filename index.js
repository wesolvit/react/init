#!/usr/bin/env node
'use strict';
const path = require('path');
const program = require('commander');
const inquirer = require('inquirer');
const fs = require('fs');
const fse = require('fs-extra');
const marked = require('marked');
const TerminalRenderer = require('marked-terminal');

const info = require('./helpers/info');

const packageJson = require('./package.json');
const {
  getFormattedFlags,
  getFormattedDefaultValue,
  getFormattedValueName,
} = require('./helpers/options-helpers');
const ensureEmptiness = require('./helpers/ensure-emptiness');
const getQuestionWithDefault = require('./helpers/question-default');
const fileHandler = require('./helpers/file-handling');

program
  .description("Create a react application project", {
    directory: "The location where to create the project."
  })
  .version(packageJson.version)
  .arguments('<directory>')
  .action(directory => {
    const directoryFullPath = path.resolve(directory);
    fse.ensureDir(directory)
      .then(() => {
        fse.readdir(directory, (err, files) => {
          ensureEmptiness(err, files, directory);
          const questions = getQuestionWithDefault(program, directory, info);
          inquirer
            .prompt(questions)
            .then(function (answers) {
              const resourcesDir = path.resolve(__dirname, 'resources');
              fse.readdir(resourcesDir, fileHandler(directory, directoryFullPath, answers));
            });
        });
      });
  });

info.options.forEach(option => {
  const {short, long, isFlag, name, description, default: defaultValue} = option;
  program.option(
    `${getFormattedFlags(short, long)}${getFormattedValueName(isFlag, name)}`,
    `${description}${getFormattedDefaultValue(defaultValue)}`
  );
});

program.option('--readme', 'Show README');

// Ignore argument and options when the readme option is provided.
if(process.argv.includes('--readme')) {
  program.outputHelp(() => {
    marked.setOptions({
      renderer: new TerminalRenderer(),
      mangle: false,
    });
    const readme = fs.readFileSync(path.resolve(__dirname, 'README.md')).toString();

    return marked(readme);
  })
}
else {
  program.parse(process.argv);
}
