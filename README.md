<div style="text-align: center">
  <a href="https://gitlab.com/wesolvit/react/init">
    <img src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/9872690/logo.png?width=80" alt="Logo" width="80" height="80" />
  </a>

  <h3>WeSolvIT React init</h3>

  <p>
    An alternative to "create-react-app".
  </p>
</div>

## Table of Contents

* [About this project](#about-this-project)
  * [Built with](#built-with)
* [Getting Started](#getting-started)
* [Usage](#usage)
  * [Docker support](#docker-support)
* [Future](#future)
* [License](#license)
* [Contact](#contact)

## About this project

Most of the React developers are using "create-react-app" to start a new project.\
But it becomes quickly a nightmare when you want to add some customization to your project.\
Some extra libraries needs to be added to override the default configuration, or you have to eject the project.

This project initiator will allow you to have more control on your project using webpack, babel, sass, typescript and eslint.

It supports Docker environment, by creating either a docker-compose or just aliases to docker run with your favorite Docker image.

Since version 1.0.6, it supports typescript. You can have a mix of js and ts in the project.\
By default you cannot import js file into a ts, you need to allow js in the tsconfig.js, but you can import ts into js.

<p style="text-align: center">
![Terminal wesolvit-react-init](doc/images/terminal-wesolvit-react-init.gif)
</p>


### Built with

* [Commander.js](https://www.npmjs.com/package/commander)
* [Inquirer.js](https://www.npmjs.com/package/inquirer)
* [spdx-license-list](https://www.npmjs.com/package/spdx-license-list)


## Getting Started

```npm
npm -g install wesolvit-react-init
wesolvit-react-init my-neww-react-app-directory
```

## Usage

The tool will ask you several questions about your project. You can provide some default values via the command line.\
Here are the options:
```
Usage: wesolvit-react-init [options] <directory>

Options:
  -V, --version                                    output the version number
  -n, --project-name <name>                        The project name
  -v, --project-version <version>                  The project version (default: 0.0.1)
  -d, --desc <description>                         The project description
  -a, --author <author>                            The project author
  -l, --license <license>                          The project license (default: GPL-3.0-or-later)
  -c, --docker                                     Use docker (default: false)
  --docker-type <dockerType>                       Docker type (default: docker-compose)
  --docker-compose-image <dockerComposeImage>      Image for docker-compose (default: node:alpine)
  --docker-compose-context <dockerComposeContext>  Build context for docker-compose
  --docker-run-image <dockerRunImage>              Docker image (default: node:alpine)
  --docker-exec-container <dockerExecContainer>    Docker container (default: node)
  -h, --help                                       display help for command
```

Examples:
```sh
wesolvit-react-init . # Will create the project in current directory if it is empty
wesolvit-react-init my-project --docker --docker-type="docker run image" docker-run-image="node:13"
# Will create a new directory my-project and allow you to generate aliases to run node:13 with docker.
```

### Docker support ###
There are 4 way to use Docker.
1. With docker-compose and an image as node service.
1. With docker-compose and a build context.
1. With docker run, and a Docker image.
1. With docker exec, and a Docker container.

For those, the tool will create a .aliases file with aliases to `node`, `npm`, `npx`, `yarn`, `webpack`.\
Also `bash-node` to access the bash of the container and `serve` to start webpack development server, the equivalent to `yarn start` in `create-react-app`.

The docker approach allows you to have different versions of node, npm or yarn for different projects based on the Docker image or container your are running.

Example with docker:
```sh
wesolvit-react-init my-project --docker --docker-type="docker run image" docker-run-image="node:13"
cd my-project # Go to the project directory.
. .aliases # Load the aliases.
yarn install # Install the libraries.
serve # Start the development server

# Equivalent without the aliases

docker run --rm -v $(pwd):/app/build -w /app/build node:13 yarn install # yarn install
docker run --rm -p 8080:8080 -v $(pwd):/app/build -w /app/build node:13 yarn serve # serve
```

Example with docker-compose:
```sh
wesolvit-react-init my-project --docker --docker-type="docker-compose context" docker-compose-context="/path/to/a/docker/build/context/with/Dockerfile"
# This will copy the Dockerfile and all the files needed for that Dockerfile.
cd my-project # Go to the project directory.
. .aliases # Load the aliases.
docker-compose up -d --build
yarn install # Install the libraries.
serve # Start the development server

# Equivalent without the aliases

docker-compose exec node yarn install # yarn install
docker-compose exec node yarn serve # serve
```

**Feel free to adapt the docker-compose, and the aliases files to your usage.**

## Future

What's coming next

* No interactive mode for CI support
* Testing support

## License

Distributed under the *GNU GENERAL PUBLIC LICENSE Version 3*, License. See `LICENSE` for more information.

## Contact

Rachid TARSIMI - [![LinkedIn][linkedin-shield]][linkedin-url] - rachid@wesolv.it

Project Link: [https://gitlab.com/wesolvit/react/init](https://gitlab.com/wesolvit/react/init)

<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/rachid-tarsimi
